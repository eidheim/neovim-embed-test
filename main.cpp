#include "process.hpp"
#include <msgpack.hpp>
#include <iostream>
#include <mutex>
#include <future>

using namespace std;

class NVim {
  std::map<size_t, std::function<void(const msgpack::v2::object&)>> handlers;
  std::mutex handlers_mutex;
  std::unique_ptr<TinyProcessLib::Process> process;
public:
  uint64_t request_id=0;
  std::function<void(const msgpack::v2::object&)> on_event;
  
  void start() {
    process=std::make_unique<TinyProcessLib::Process>("nvim --embed", "", [this](const char *bytes, size_t n) {
      auto object_handle=msgpack::unpack(bytes, n);
      auto object=object_handle.get();
      msgpack::type::tuple<uint64_t> dst;
      object.convert(dst);
      if(dst.get<0>()==1) {
        msgpack::type::tuple<uint64_t, uint64_t> dst2;
        object.convert(dst2);
        std::unique_lock<std::mutex> lock(handlers_mutex);
        auto it=handlers.find(dst2.get<1>());
        if(it!=handlers.end()) {
          auto handler=std::move(it->second);
          handlers.erase(it);
          lock.unlock();
          handler(object);
        }
      }
      else if(dst.get<0>()==2) {
        if(on_event)
          on_event(object);
      }
    }, [](const char *bytes, size_t n) {
      cerr.write(bytes, n);
      cerr << endl;
    }, true, 1048576);
  }
  
  void send(const msgpack::sbuffer &buffer, std::function<void(const msgpack::v2::object&)> handler=nullptr) {
    if(handler) {
      std::unique_lock<std::mutex> lock(handlers_mutex);
      handlers.emplace(request_id-1, handler);
    }
    process->write(buffer.data(), buffer.size());
  }
};

int main() {
  NVim nvim;
  nvim.on_event=[](const msgpack::v2::object &object) {
    cout << "event: " << object << endl;
  };
  nvim.start();
  this_thread::sleep_for(chrono::seconds(1)); // TODO: remove (nvim is bugged)
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_buf_attach";
    packer.pack_array(3) << 0 << false;
    packer.pack_map(0);
    
    cout << "Calling nvim_buf_attach" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_buf_line_count";
    packer.pack_array(1) << 0;
    
    cout << "Calling nvim_buf_line_count" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
    
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_buf_set_lines";
    packer.pack_array(5) << 0 << 0 << 0 << 0;
    packer.pack_array(3) << "line1" << "line2" << "line3";
    
    cout << "Calling nvim_buf_set_lines" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_buf_line_count";
    packer.pack_array(1) << 0;
    
    cout << "Calling nvim_buf_line_count" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_win_set_cursor";
    packer.pack_array(2) << 0;
    packer.pack_array(2) << 1 << 1;
    
    cout << "Calling nvim_win_set_cursor" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_get_current_line";
    packer.pack_array(0);
    
    cout << "Calling nvim_get_current_line" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_input";
    packer.pack_array(1) << "dd";
    
    cout << "Calling nvim_input" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_get_current_line";
    packer.pack_array(0);
    
    cout << "Calling nvim_get_current_line" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }
  
  {
    msgpack::sbuffer buffer;
    msgpack::packer<msgpack::sbuffer> packer(&buffer);
    packer.pack_array(4) << 0 << nvim.request_id++ << "nvim_buf_get_lines";
    packer.pack_array(4) << 0 << 0 << 3 << true;
    
    cout << "Calling nvim_buf_get_lines" << endl;
    promise<void> result_processed;
    nvim.send(buffer, [&result_processed](const msgpack::v2::object &object) {
      cout << "result: " << object << endl;
      result_processed.set_value();
    });
    result_processed.get_future().get();
  }  
}
